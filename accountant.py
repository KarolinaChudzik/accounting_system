import sys

komendy = sys.argv[1]
saldo = 1000.0
magazyn_lista = {}

DOZWOLONE_WYWOLANIA = ("saldo", "sprzedaz", "zakup", "przeglad")
LISTA_DOZWOLONYCH_AKCJI = ("saldo", "sprzedaz", "zakup", "stop")
logs = [] #historia operacji
parametry = [] #lista parametrow

while True:
    akcja_rodzaj = input("Podaj rodzaj akcji:")
    if not akcja_rodzaj in LISTA_DOZWOLONYCH_AKCJI:
        print("Niedozwolona komenda, podaj prawidlowa komende.")
        continue
    elif akcja_rodzaj == "stop":
        print("Zakonczyles prace programu.")
        break
    elif akcja_rodzaj == "saldo":
        konto_zmiana_saldo = float(input("Podaj wartosc zmiany na koncie (gr):"))
        zmiana_komentarz_saldo = input("Podaj komentarz do zmiany:")
        paratmert = ("Rodzaj akcji:{} "
                     "Wartosc zmiany na koncie (gr):{}. "
                     "Komentarz do zmiany: {}. ".
                     format(akcja_rodzaj,konto_zmiana_saldo, zmiana_komentarz_saldo))
        parametry.append(paratmert)
        if (konto_zmiana_saldo) < 0 and (saldo + konto_zmiana_saldo) < 0:
                print("Podana wartosc przewyzsza saldo, wpisz wartosc ponownie.")
                continue
        saldo = saldo + konto_zmiana_saldo
        log = f"Zmiana saldo: {konto_zmiana_saldo} z komentarzem: {zmiana_komentarz_saldo}."
        logs.append(log)
    elif akcja_rodzaj == "zakup":
        identyfikator_produktu_zakup = input(("Podaj identyfikator produktu:"))
        cena_jedn_zakup = float(input("Podaj cene jednostkowa:"))
        ilosc_szt_zakup = int(input("Podaj ilosc szt:"))
        paratmert = ("Rodzaj akcji: {} "
                     "Identyfikator produktu:{}. "
                     "Cene jednostkowa: {}. "
                     "Ilosc szt:{}".
                     format(akcja_rodzaj, identyfikator_produktu_zakup, cena_jedn_zakup, ilosc_szt_zakup))
        parametry.append(paratmert)
        wartosc_zakupu = ilosc_szt_zakup * cena_jedn_zakup
        if wartosc_zakupu > saldo:
            print(f'Cena za towary ({wartosc_zakupu} przekracza wartosc salda {saldo}.)')
            continue
        else:
            saldo = saldo - wartosc_zakupu
            if not magazyn_lista.get(identyfikator_produktu_zakup):
                magazyn_lista[identyfikator_produktu_zakup] = {"ilosc": ilosc_szt_zakup, "cena": cena_jedn_zakup}
            else:
                magazyn_ilosc_prod = magazyn_lista[identyfikator_produktu_zakup]["ilosc"]
                magazyn_lista[identyfikator_produktu_zakup] = {"ilosc": magazyn_ilosc_prod + ilosc_szt_zakup,
                                                               "cena": cena_jedn_zakup}
        log = f"Dokonano zakupu produktu {identyfikator_produktu_zakup},ilosc {ilosc_szt_zakup} w cenie za szt.{cena_jedn_zakup}."
        logs.append(log)
    elif akcja_rodzaj == "sprzedaz":
        identyfikator_produktu_sprzedaz = input("Podaj identyfikator produktu:")
        cena_jedn_sprzedaz = float(input("Podaj cene jednostkowa:"))
        ilosc_szt_sprzedaz = int(input("Podaj ilosc szt:"))
        paratmert = ("Rodzaj akcji: {} " 
                     "Identyfikator produktu:{}. "
                     "Cene jednostkowa: {}. "
                     "Ilosc szt:{}".
                     format(akcja_rodzaj, identyfikator_produktu_sprzedaz, cena_jedn_sprzedaz, ilosc_szt_sprzedaz))
        parametry.append(paratmert)
        wartosc_sprzedazy = cena_jedn_sprzedaz * ilosc_szt_sprzedaz
        if not magazyn_lista.get(identyfikator_produktu_sprzedaz):
            print("Brak produktu na stanie magazynowym, podaj inny produkt")
            continue
        if magazyn_lista.get(identyfikator_produktu_sprzedaz)["ilosc"] < ilosc_szt_sprzedaz:
            print("Brak wystarczajace ilosci produktow, wprowadz inna ilosc.")
            continue
        magazyn_ilosc_prod = magazyn_lista[identyfikator_produktu_sprzedaz]["ilosc"]
        magazyn_lista[identyfikator_produktu_sprzedaz] = {
            "ilosc": magazyn_ilosc_prod - ilosc_szt_sprzedaz,
            "cena": cena_jedn_sprzedaz
        }
        saldo = saldo + wartosc_sprzedazy
        if not magazyn_lista.get(identyfikator_produktu_sprzedaz)['ilosc']:
            del magazyn_lista[identyfikator_produktu_sprzedaz]
        log = f"Dokonano srzedazy produktu {identyfikator_produktu_sprzedaz},ilosc {ilosc_szt_sprzedaz} w cenie za szt.{cena_jedn_sprzedaz}."
        logs.append(log)


if komendy == "konto":
    print(f"Saldo:{saldo}")
elif komendy == "magazyn":
    magazyn = (sys.argv[2:])
    for i in magazyn:
        magazyn_lista[i] = {
            "ilosc": 0,
            "cena": 0}
    print(f"Stan magazynu:{magazyn_lista}")
elif komendy == "przeglad":
    print(logs)
elif komendy == "saldo":
    wartosc_zmiany = int(sys.argv[2])
    komentarz = sys.argv[3]
    if (wartosc_zmiany) < 0 and (saldo + wartosc_zmiany) < 0:
        print("Podana wartosc przewyzsza saldo, wpisz wartosc ponownie.")
    saldo = saldo + wartosc_zmiany
    paratmert = ("Rodzaj akcji: {} "
                 "Wartosc zmiany na koncie (gr):{}. "
                 "Komentarz do zmiany: {}. ".
                 format(sys.argv[1], wartosc_zmiany, komentarz))
    parametry.append(paratmert)
elif komendy == "sprzedaz":
    identyfikator_sprzedaz = sys.argv[2]
    cena_sprzedaz = int(sys.argv[3])
    ilosc_sprzedaz = int(sys.argv[4])
    paratmert = ("Rodzaj akcji: {} "
                 "Identyfikator produktu:{}. "
                 "Cene jednostkowa: {}. "
                 "Ilosc szt:{}".
                 format(sys.argv[1], identyfikator_sprzedaz, cena_sprzedaz, ilosc_sprzedaz))
    parametry.append(paratmert)
    wartosc_sprzedazy_koniec = cena_sprzedaz * ilosc_sprzedaz
    if not magazyn_lista.get(identyfikator_sprzedaz) or magazyn_lista.get(identyfikator_sprzedaz)["ilosc"] < ilosc_sprzedaz:
        print("Brak produktu na stanie lub niewystrczajaca ilosc produktow, aby dokonac sprzedazy.")
    magazyn_ilosc_prod = magazyn_lista[identyfikator_sprzedaz]["ilosc"]
    magazyn_lista[identyfikator_sprzedaz] = {
        "ilosc": magazyn_ilosc_prod - ilosc_sprzedaz,
        "cena": cena_sprzedaz
    }
    saldo = saldo + wartosc_sprzedazy_koniec
    if not magazyn_lista.get(identyfikator_sprzedaz)['ilosc']:
        del magazyn_lista[identyfikator_sprzedaz]
elif komendy == "zakup":
    identyfikator_zakup = str(sys.argv[2])
    cena_zakup = int(sys.argv[3])
    ilosc_zakup = int(sys.argv[4])
    wartosc_zakupu_koniec = ilosc_zakup * cena_zakup
    paratmert = ("Rodzaj akcji: {} "
                 "Identyfikator produktu:{}. "
                 "Cene jednostkowa: {}. "
                 "Ilosc szt:{}".
                 format(sys.argv[1], identyfikator_zakup, cena_zakup, ilosc_zakup))
    parametry.append(paratmert)
    if wartosc_zakupu_koniec > saldo:
        print(f'Cena za towary ({wartosc_zakupu_koniec} przekracza wartosc salda {saldo}.)')
    else:
        saldo = saldo - wartosc_zakupu_koniec
    if not magazyn_lista.get(identyfikator_zakup):
        magazyn_lista[identyfikator_zakup] = {"ilosc": ilosc_zakup, "cena": cena_zakup}
    else:
        magazyn_ilosc_prod = magazyn_lista[identyfikator_zakup]["ilosc"]
        magazyn_lista[identyfikator_zakup] = {"ilosc": magazyn_ilosc_prod + ilosc_zakup,
                                                       "cena": cena_zakup}
for podane_parametry in enumerate(parametry):
    if komendy in DOZWOLONE_WYWOLANIA:
        print(f"Podane parametry:{podane_parametry}")
